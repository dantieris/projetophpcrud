<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Cadastrar Usuario</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarUsuarios.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarUsuario.php" method="post">
				<fieldset>
					<label for="email">Email: <input name="email" id="email" type="email"/></label>
					<label for="senha">Senha: <input name="senha" id="senha" type="password"/></label>
					<label for="senha_repetida">Repita a Senha: <input name="senha_repetida" id="senha_repetida" type="password"/></label>
					<input id="cadastrar" name="acao" type="submit" value="Cadastrar"/>
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>