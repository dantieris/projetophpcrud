<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Tela Principal</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarUsuarios.php"); // Importando a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<figure>
				<img src="imagens/Financeiro1.jpg" id="imagem_principal">
			</figure>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>