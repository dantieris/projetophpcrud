<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Remover Devedor</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarDevedores.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarDevedor.php" method="post">
				<fieldset>
					<label for="id_devedor">Devedor: 
						<select name="id_devedor" id="id_devedor">
						<?php
							include "classes/DevedorDAO.class.php";
							$dao = new DevedorDAO();
							$dao->montaComboDevedores();
						?>
						</select>
					<input id="remover" name="acao" type="submit" value="Remover" />
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>