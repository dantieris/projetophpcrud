<nav>
	<link rel="stylesheet" type="text/css" href="style/nav.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset -->
	
	<header>
		<h1>Menu Principal</h1>
		<ul>
			<li><a href="cadastrarDevedorForm.php">Cadastrar Devedor</a></li>
			<li><a href="atualizarDevedorForm.php">Atualizar Devedor</a></li>
			<li><a href="removerDevedorForm.php">Remover Devedor</a></li>
		</ul>
	</header>
</nav>