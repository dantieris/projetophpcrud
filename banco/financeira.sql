-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06-Dez-2014 às 18:32
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `financeira`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `credores`
--

CREATE TABLE IF NOT EXISTS `credores` (
`id_credor` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(10) DEFAULT NULL,
  `cpf_cnpj` varchar(15) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `devedores`
--

CREATE TABLE IF NOT EXISTS `devedores` (
`id_devedor` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(10) DEFAULT NULL,
  `cpf_cnpj` varchar(14) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `titulos`
--

CREATE TABLE IF NOT EXISTS `titulos` (
`id_titulo` int(11) NOT NULL,
  `valor` float NOT NULL,
  `numero` int(11) NOT NULL,
  `parcela` int(11) NOT NULL,
  `data_vencimento` date NOT NULL,
  `data_pagamento` date NOT NULL,
  `id_devedor` int(11) NOT NULL,
  `id_credor` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `data_emissao` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id_usuario` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `email`, `senha`) VALUES
(1, 'teste@teste.com', '827ccb0eea8a706c4c34a16891f84e7b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credores`
--
ALTER TABLE `credores`
 ADD PRIMARY KEY (`id_credor`), ADD UNIQUE KEY `cpf/cnpj` (`cpf_cnpj`), ADD KEY `id_usuario` (`id_usuario`), ADD KEY `id_usuario_2` (`id_usuario`);

--
-- Indexes for table `devedores`
--
ALTER TABLE `devedores`
 ADD PRIMARY KEY (`id_devedor`), ADD UNIQUE KEY `cpf/cnpj` (`cpf_cnpj`), ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `titulos`
--
ALTER TABLE `titulos`
 ADD PRIMARY KEY (`id_titulo`), ADD KEY `id_usuario` (`id_usuario`), ADD KEY `id_devedor` (`id_devedor`,`id_credor`,`id_usuario`), ADD KEY `id_credor` (`id_credor`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id_usuario`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credores`
--
ALTER TABLE `credores`
MODIFY `id_credor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `devedores`
--
ALTER TABLE `devedores`
MODIFY `id_devedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `titulos`
--
ALTER TABLE `titulos`
MODIFY `id_titulo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `credores`
--
ALTER TABLE `credores`
ADD CONSTRAINT `credores_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `devedores`
--
ALTER TABLE `devedores`
ADD CONSTRAINT `devedores_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `titulos`
--
ALTER TABLE `titulos`
ADD CONSTRAINT `titulos_ibfk_1` FOREIGN KEY (`id_devedor`) REFERENCES `devedores` (`id_devedor`),
ADD CONSTRAINT `titulos_ibfk_2` FOREIGN KEY (`id_credor`) REFERENCES `credores` (`id_credor`),
ADD CONSTRAINT `titulos_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
