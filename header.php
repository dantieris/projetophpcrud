<header>
	<link rel="stylesheet" type="text/css" href="style/header.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset (Não tenho certeza se é a melhor mandeira de fazer isso) -->

	<a href="main.php">
		<img id="logo" src="imagens/logo.png">
	</a>

	<div id="logout">
		Sessão ativa por: 
		<?php 
			session_start();
			echo $_SESSION["usuario"]->email; 
		?>
		<button name="logout" type="button" onclick="document.location.href='logout.php';">Logout</button>
	</div>
</header>