<?php
	
	// verifica se as informações vieram 
	// através do método POST
	if(isset($_POST))
	{
		require_once("classes/Devedor.class.php");
		require_once("classes/DevedorDAO.class.php");

		switch ($_POST["acao"]) {
			case 'Cadastrar':
				// recebe os valores vindos do formulário através de post
				$cpf_cnpj = $_POST["cpf_cpnj"];
				$email = $_POST["email"];
				$telefone = $_POST["telefone"];
				$razao_social = $_POST["razao_social"];
				$endereco = $_POST["endereco"];

				$devedor = new Devedor("", $cpf_cnpj, $email, $telefone, $razao_social, $endereco);
				$acoes = new DevedorDAO();

				if($acoes) {
					echo "Cadastrando... <br/>";
				}

				$acoes->inserir($devedor);
				break;
			case 'Atualizar' :
					// recebe os valores vindos do formulário através de post
					$cpf_cnpj = $_POST["cpf_cpnj"];
					$email = $_POST["email"];
					$telefone = $_POST["telefone"];
					$razao_social = $_POST["razao_social"];
					$endereco = $_POST["endereco"];
					$id = $_POST["id_devedor"];
					
					$devedor = new Devedor($id, $cpf_cnpj, $email, $telefone, $razao_social, $endereco);
					$acoes = new DevedorDAO();

					if($acoes) {
						echo "Atualizando... <br/>";
					}

					$acoes->atualizar($devedor);
				break;
			case 'Remover' :
					$id = $_POST["id_devedor"];

					$devedor = new Devedor($id, "", "", "", "", "");
					$acoes = new DevedorDAO();

					if($acoes) {
						echo "Removerndo... <br/>";
					}

					$acoes->remover($devedor);
				break;
			default:
				# code...
				break;
		}
	}
?>