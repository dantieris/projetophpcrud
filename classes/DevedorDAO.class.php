<?php 

require_once("BancoPDO.class.php");

class DevedorDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}

	public function inserir($devedor) {
		try { 
			$stm = $this->conexao->prepare("SELECT cpf_cnpj 
											FROM devedores 
											WHERE cpf_cnpj = ?");

			$stm->bindValue(1, $devedor->cpf_cnpj);
			$verificaCNPJ = $stm->execute();

			if ($stm->rowCount() > 0) {
				echo "Este CPF/CNPJ já está cadastrado.<br/>";
				echo "Redirecionando para a tela de Cadastrar Devedor em 5 segundos.";
				header("refresh:5; url=cadastrarDevedorForm.php");
			} else {
				$stm = $this->conexao->prepare("INSERT INTO devedores 
												(cpf_cnpj, email, telefone, id_usuario, razao_social, endereco) 
												VALUES (?, ?, ?, ?, ?, ?)");

				session_start();

				$cpf_cnpj = $devedor->cpf_cnpj;
				$email = $devedor->email;
				$telefone = $devedor->telefone;
				$id_usuario = $_SESSION["usuario"]->id_usuario;
				$razao_social = $devedor->razao_social;
				$endereco = $devedor->endereco;

				$stm->bindValue(1, $cpf_cnpj);
				$stm->bindValue(2, $email);
				$stm->bindValue(3, $telefone);
				$stm->bindValue(4, $id_usuario);
				$stm->bindValue(5, $razao_social);
				$stm->bindValue(6, $endereco);

				$stm->execute();
				
				echo "Dados inseridos com sucesso!<br/>";
				echo "Redirecionando para a tela de Cadastrar Devedor em 5 segundos.";
				header("refresh:5; url=cadastrarDevedorForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function atualizar($devedor) {
		// Verifica se o devedor está cadastrado
		try { 
			$stm = $this->conexao->prepare("SELECT *
											FROM devedores 
											WHERE id_devedor = ?");

			$stm->bindValue(1, $devedor->id);

			$query = $stm->execute();

			if ($query) {

				$dados = $stm->fetch(PDO::FETCH_OBJ);

				$stm = $this->conexao->prepare("UPDATE devedores 
												SET email = ?, telefone = ?, cpf_cnpj = ?, razao_social = ?, endereco = ?
												WHERE id_devedor = ?");

				session_start();

				$cpf_cnpj = $devedor->cpf_cnpj;
				$email = $devedor->email;
				$telefone = $devedor->telefone;
				$razao_social = $devedor->razao_social;
				$endereco = $devedor->endereco;
				$id_devedor = $devedor->id;

				if ($cpf_cnpj == null) {
					$cpf_cnpj = $dados->cpf_cnpj;
				}

				if ($email == null) {
					$email = $dados->email;
				}

				if ($telefone == null) {
					$telefone = $dados->telefone;
				}

				if ($razao_social == null) {
					$razao_social = $dados->razao_social;
				}

				if ($endereco == null) {
					$endereco = $dados->endereco;
				}

				$stm->bindValue(1, $email);
				$stm->bindValue(2, $telefone);
				$stm->bindValue(3, $cpf_cnpj);
				$stm->bindValue(4, $razao_social);
				$stm->bindValue(5, $endereco);
				$stm->bindValue(6, $id_devedor);

				$stm->execute();
				
				echo "Dados atualizados com sucesso!<br/>";
				echo "Redirecionando para a tela de Atualizar devedor em 5 segundos.";
				header("refresh:5; url=atualizarDevedorForm.php");
			} else {
				echo "Este CPF/CNPJ ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Atualizar devedor em 5 segundos.";
				header("refresh:5; url=atualizarDevedorForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function visualizar() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM devedores");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "Devedor ID ".$dados->id_devedor." - Email ".$dados->email.
						" - CPF/CPNJ ".$dados->cpf_cnpj." - Telefone ".$dados->telefone."<br/>";
				}
			}

			} catch (PDOException $e) {
				echo "Erro: ".$e->getMessage();
			}
	}

	public function remover($devedor) {
		try { 
			$stm = $this->conexao->prepare("SELECT id_devedor 
											FROM devedores 
											WHERE id_devedor = ?");

			$stm->bindValue(1, $devedor->id);
			$verificaId_devedor = $stm->execute();

			if ($stm->rowCount() > 0) {
				$stm = $this->conexao->prepare("DELETE FROM devedores
												WHERE id_devedor = ?");

				$stm->bindValue(1, $devedor->id);
				$stm->execute();

				echo "Dados removidos com sucesso!<br/>";
				echo "Redirecionando para a tela de Remover devedor em 5 segundos.";
				header("refresh:5; url=removerDevedorForm.php");
			} else {
				echo "Este CPF/CNPJ ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Remover devedor em 5 segundos.";
				header("refresh:5; url=removerDevedorForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	function montaComboDevedores() {

		try {

			$stm = $this->conexao->prepare("SELECT * FROM devedores");
			
			$query = $stm->execute();

			if ($query) {
				echo "<option value='0'>Selecione...</option>";
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<option value='".$dados->id_devedor."'>".$dados->email."</option>";
				}
			}
		} catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaTabelaDevedores() {
		try {
			$stm = $this->conexao->prepare("SELECT * FROM devedores");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<tr>";
					echo "<td>".$dados->id_devedor."</td>";
					echo "<td>".$dados->razao_social."</td>";
					echo "<td>".$dados->cpf_cnpj."</td>";
					echo "<td>".$dados->email."</td>";
					echo "<td>".$dados->telefone."</td>";
					echo "<td>".$dados->endereco."</td>";
					echo "</tr>";
				}
			}
		}
		catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}

	}

}

?>