<?php

include "Devedor.class.php";
include "Credor.class.php";

class Titulo {

	private $id;
	private $data_emissao;
	private $data_vencimento;
	private $data_pagamento;
	private $devedor;
	private $credor;
	private $valor;
	private $numero;
	private $parcela;
	
	function __construct($id="", $data_emissao="", $data_vencimento="", $devedor="", $credor="", 
						$valor="", $numero="", $parcela="", $data_pagamento="")
	{
		
		$this->id = $id;
		$this->data_emissao = $data_emissao;
		$this->data_vencimento = $data_vencimento;
		$this->devedor = $devedor;
		$this->credor = $credor;
		$this->valor = $valor;
		$this->numero = $numero;
		$this->parcela = $parcela;
		$this->data_pagamento = $data_pagamento;
	}

	function __set($prop, $val) {
		$this->$prop = $val;
	}

	function __get($prop) {
		return $this->$prop;
	}

	function __toString() {
		return "O titulo de id [".$this->id."] tem valor ".
			   $this->valor." com data de vencimento em ".$this->data_vencimento; 
	}
}

?>