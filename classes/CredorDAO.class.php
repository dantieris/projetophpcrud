<?php 

require_once("BancoPDO.class.php");

class CredorDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}

	public function inserir($credor) {
		try { 
			$stm = $this->conexao->prepare("SELECT cpf_cnpj 
											FROM credores 
											WHERE cpf_cnpj = ?");

			$stm->bindValue(1, $credor->cpf_cnpj);
			$verificaCNPJ = $stm->execute();

			if ($stm->rowCount() > 0) {
				echo "Este CPF/CNPJ já está cadastrado.<br/>";
				echo "Redirecionando para a tela de Cadastrar Credor em 5 segundos.";
				header("refresh:5; url=cadastrarCredorForm.php");
			} else {
				$stm = $this->conexao->prepare("INSERT INTO credores 
												(cpf_cnpj, email, telefone, id_usuario, razao_social, endereco) 
												VALUES (?, ?, ?, ?, ?, ?)");

				session_start();

				$cpf_cnpj = $credor->cpf_cnpj;
				$email = $credor->email;
				$telefone = $credor->telefone;
				$id_usuario = $_SESSION["usuario"]->id_usuario;
				$razao_social = $credor->razao_social;
				$endereco = $credor->endereco;

				$stm->bindValue(1, $cpf_cnpj);
				$stm->bindValue(2, $email);
				$stm->bindValue(3, $telefone);
				$stm->bindValue(4, $id_usuario);
				$stm->bindValue(5, $razao_social);
				$stm->bindValue(6, $endereco);

				$stm->execute();
				//echo "Último ID_USUARIO inserido ".$this->conexao->lastInsertId()."<br/>";
				
				echo "Dados inseridos com sucesso!<br/>";
				echo "Redirecionando para a tela de Cadastrar Credor em 5 segundos.";
				header("refresh:5; url=cadastrarCredorForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function atualizar($credor) {
		// Verifica se o credor está cadastrado
		try { 
			$stm = $this->conexao->prepare("SELECT *
											FROM credores 
											WHERE id_credor = ?");

			$stm->bindValue(1, $credor->id);

			$query = $stm->execute();

			if ($query) {

				$dados = $stm->fetch(PDO::FETCH_OBJ);

				$stm = $this->conexao->prepare("UPDATE credores 
												SET email = ?, telefone = ?, cpf_cnpj = ?, razao_social = ?, endereco = ?
												WHERE id_credor = ?");

				session_start();

				$cpf_cnpj = $credor->cpf_cnpj;
				$email = $credor->email;
				$telefone = $credor->telefone;
				$razao_social = $credor->razao_social;
				$endereco = $credor->endereco;
				$id_credor = $credor->id;

				if ($cpf_cnpj == null) {
					$cpf_cnpj = $dados->cpf_cnpj;
				}

				if ($email == null) {
					$email = $dados->email;
				}

				if ($telefone == null) {
					$telefone = $dados->telefone;
				}

				if ($razao_social == null) {
					$razao_social = $dados->razao_social;
				}

				if ($endereco == null) {
					$endereco = $dados->endereco;
				}

				$stm->bindValue(1, $email);
				$stm->bindValue(2, $telefone);
				$stm->bindValue(3, $cpf_cnpj);
				$stm->bindValue(4, $razao_social);
				$stm->bindValue(5, $endereco);
				$stm->bindValue(6, $id_credor);

				$stm->execute();
				
				echo "Dados atualizados com sucesso!<br/>";
				echo "Redirecionando para a tela de Atualizar Credor em 5 segundos.";
				header("refresh:5; url=atualizarCredorForm.php");
			} else {
				echo "Este CPF/CNPJ ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Atualizar Credor em 5 segundos.";
				header("refresh:5; url=atualizarCredorForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function visualizar() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM credores");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "Credor ID ".$dados->id_credor." - Email ".$dados->email.
						" - CPF/CPNJ ".$dados->cpf_cnpj." - Telefone ".$dados->telefone."<br/>";
				}
			}

			} catch (PDOException $e) {
				echo "Erro: ".$e->getMessage();
			}
	}

	public function remover($credor) {
		try { 
			$stm = $this->conexao->prepare("SELECT id_credor 
											FROM credores 
											WHERE id_credor = ?");

			$stm->bindValue(1, $credor->id);
			$verificaId_credor = $stm->execute();

			if ($stm->rowCount() > 0) {
				$stm = $this->conexao->prepare("DELETE FROM credores
												WHERE id_credor = ?");

				$stm->bindValue(1, $credor->id);
				$stm->execute();

				echo "Dados removidos com sucesso!<br/>";
				echo "Redirecionando para a tela de Remover Credor em 5 segundos.";
				header("refresh:5; url=removerCredorForm.php");
			} else {
				echo "Este CPF/CNPJ ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Remover Credor em 5 segundos.";
				header("refresh:5; url=removerCredorForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaComboCredores() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM credores");
			
			$query = $stm->execute();

			if ($query) {
				echo "<option value='0'>Selecione...</option>";
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {

					echo "<option value='".$dados->id_credor."'>".$dados->email."</option>";
				}
			}
		} catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaTabelaCredores() {
		try {
			$stm = $this->conexao->prepare("SELECT * FROM credores");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<tr>";
					echo "<td>".$dados->id_credor."</td>";
					echo "<td>".$dados->razao_social."</td>";
					echo "<td>".$dados->cpf_cnpj."</td>";
					echo "<td>".$dados->email."</td>";
					echo "<td>".$dados->telefone."</td>";
					echo "<td>".$dados->endereco."</td>";
					echo "</tr>";
				}
			}
		}
		catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}

	}

}

?>