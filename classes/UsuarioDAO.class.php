﻿<?php

require_once("BancoPDO.class.php");

class UsuarioDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}
	public function inserir($usuario) {

		try { 

			$stm = $this->conexao->prepare("SELECT email 
											FROM usuarios 
											WHERE email = ?");

			$stm->bindValue(1, $usuario->email);
			$verificaEmail = $stm->execute();

			if ($stm->rowCount() > 0) {
				echo "Este e-mail já esta cadastrado.<br/>";
				echo "Redirecionando para a tela de Cadastrar Usuário em 5 segundos.";
				header("refresh:5; url=cadastrarUsuarioForm.php");
			} else {
				$stm = $this->conexao->prepare("INSERT INTO usuarios 
												(email, senha, id_usuario_responsavel) 
												VALUES (?, ?, ?)");

				session_start();

				$email = $usuario->email;
				$senha = $usuario->senha;
				$id_usuario_responsavel = $_SESSION["usuario"]->id_usuario;

				$stm->bindValue(1, $email);
				$stm->bindValue(2, $senha);
				$stm->bindValue(3, $id_usuario_responsavel);

				$stm->execute();

				echo "Dados inseridos com sucesso!<br/>";
				echo "Redirecionando para a tela de Cadastrar Usuário em 5 segundos.";
				header("refresh:5; url=cadastrarUsuarioForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function atualizar($usuario) {
		// Verifica se o usuario está cadastrado
		try { 
			$stm = $this->conexao->prepare("SELECT *
											FROM usuarios 
											WHERE id_usuario = ?");

			$stm->bindValue(1, $usuario->id);

			$query = $stm->execute();

			if ($query) {
				$dados = $stm->fetch(PDO::FETCH_OBJ);

				$stm = $this->conexao->prepare("UPDATE usuarios 
												SET senha = ?
												WHERE id_usuario = ?");

				$senha = $usuario->senha;
				$id_usuario = $usuario->id;

				if ($senha == null) {
					$senha = $dados->senha;
				}

				$stm->bindValue(1, $senha);
				$stm->bindValue(2, $id_usuario);

				$stm->execute();
				
				echo "Dados atualizados com sucesso!<br/>";
				echo "Redirecionando para a tela de Atualizar Usuário em 5 segundos.";
				header("refresh:5; url=atualizarUsuarioForm.php");
			} else {
				echo "Este e-mail ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Atualizar Usuário em 5 segundos.";
				header("refresh:5; url=atualizarUsuarioForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function visualizar($id = "") {

		try {

			if ($id == "") {
				$stm = $this->conexao->prepare("SELECT * FROM usuarios");
			} else {
				$stm = $this->conexao->prepare("SELECT * FROM usuarios
												WHERE id_usuario = ?");

				$stm->bindValue(1,$id);
			}

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo $dados->id." com email ".$dados->email;
				}
			}

		} catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function autenticar($usuario) {

		try {

			$stm = $this->conexao->prepare("SELECT * 
											FROM usuarios
											WHERE email = ?
											AND senha = ?");

			$stm->bindValue(1, $usuario->email);
			$stm->bindValue(2, $usuario->senha);

			$stm->execute();

			if($stm->rowCount() == 0) {
				return null;
			} else {
				$dados = $stm->fetch(PDO::FETCH_OBJ);

				return $dados;
			}

		} catch (PDOException $e) {
			echo "Erro autenticar UsuarioDAO: ".$e->getMessage();
		}
	}

	public function remover($usuario) {
		try { 
			$stm = $this->conexao->prepare("SELECT id_usuario 
											FROM usuarios 
											WHERE id_usuario = ?");

			$stm->bindValue(1, $usuario->id);
			$verificaId_usuario = $stm->execute();

			if ($stm->rowCount() > 0) {
				$stm = $this->conexao->prepare("DELETE FROM usuarios
												WHERE id_usuario = ?");

				$stm->bindValue(1, $usuario->id);
				$stm->execute();

				echo "Dados removidos com sucesso!<br/>";
				echo "Redirecionando para a tela de Remover Usuário em 5 segundos.";
				header("refresh:5; url=removerUsuarioForm.php");
			} else {
				echo "Este e-mail ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Remover Usuário em 5 segundos.";
				header("refresh:5; url=removerUsuarioForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	function montaComboUsuarios() {

		try {

			$stm = $this->conexao->prepare("SELECT * FROM usuarios");
			
			$query = $stm->execute();

			if ($query) {
				echo "<option value='0'>Selecione...</option>";
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<option value='".$dados->id_usuario."'>".$dados->email."</option>";
				}
			}
		} catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaTabelaUsuarios() {
		try {
			$stm = $this->conexao->prepare("SELECT * FROM usuarios");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<tr>";
					echo "<td>".$dados->id_usuario."</td>";
					echo "<td>".$dados->email."</td>";
					echo "</tr>";
				}
			}
		}
		catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}

	}


	
}

?>