<?php 

include "BancoPDO.class.php";

class TituloDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}

		public function inserir($titulo) {
		try {
			$stm = $this->conexao->prepare("SELECT *
											FROM titulos 
											WHERE numero = ?
											AND parcela = ?
											AND id_devedor = ?
											AND id_credor = ?
											AND valor = ?
											AND data_vencimento = ?
											AND data_emissao = ?");

			//print_r($titulo->titulo);
			$stm->bindValue(1, $titulo->numero);
			$stm->bindValue(2, $titulo->parcela);
			$stm->bindValue(3, $titulo->devedor->id);
			$stm->bindValue(4, $titulo->credor->id);
			$stm->bindValue(5, $titulo->valor);
			$stm->bindValue(6, $titulo->data_vencimento);
			$stm->bindValue(7, $titulo->data_emissao);

		
			$verificaTitulo = $stm->execute();

			if ($stm->rowCount() > 0) {
				echo "Este título já está cadastrado.<br/>";
				echo "Redirecionando para a tela de Cadastrar Título em 5 segundos.";
				header("refresh:5; url=cadastrarTituloForm.php");
			} else {
				$stm = $this->conexao->prepare("INSERT INTO titulos(valor, numero, parcela, data_vencimento, data_pagamento, id_devedor, id_credor, id_usuario, data_emissao) 
												VALUES (?,?,?,?,?,?,?,?,?)");
				session_start();

				$data_emissao = $titulo->data_emissao;
				$data_vencimento = $titulo->data_vencimento;
				$id_usuario = $_SESSION["usuario"]->id_usuario; // Pegando o usuario que esta logado.
				$id_devedor = $titulo->devedor->id;
				$id_credor = $titulo->credor->id;
				$valor = $titulo->valor;
				$numero = $titulo->numero;
				$parcela = $titulo->parcela;

				$stm->bindValue(1, $valor);
				$stm->bindValue(2, $numero);
				$stm->bindValue(3, $parcela);
				$stm->bindValue(4, $data_vencimento);
				$stm->bindValue(5, null);
				$stm->bindValue(6, $id_devedor);
				$stm->bindValue(7, $id_credor);
				$stm->bindValue(8, $id_usuario);
				$stm->bindValue(9, $data_emissao);

				print_r($titulo);

				$stm->execute();

				echo "Dados inseridos com sucesso! <br/>";
				echo "Redirecionando para a tela de Cadastrar titulo em 5 segundos.";
				//header("refresh:5; url=cadastrarTituloForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}
	public function atualizar($titulo) {
		// Verifica se o titulo está cadastrado
		try { 
			$stm = $this->conexao->prepare("SELECT *
											FROM titulos 
											WHERE id_titulo = ?");

			$stm->bindValue(1, $titulo->id);

			$query = $stm->execute();

			if ($query) {

				$dados = $stm->fetch(PDO::FETCH_OBJ);

				$stm = $this->conexao->prepare("UPDATE titulos 
												SET data_emissao = ?, data_vencimento = ?, valor = ?, numero = ?, parcela = ?
												WHERE id_titulo = ?");

				var_dump($dados);
				print_r($dados);

				$data_emissao = $titulo->data_emissao;
				$data_vencimento = $titulo->data_vencimento;
				$id_titulo = $titulo->id;
				$valor = $titulo->valor;
				$numero = $titulo->numero;
				$parcela = $titulo->parcela;

				if ($data_emissao == null) {
					$data_emissao = $dados->data_emissao;
				}

				if ($data_vencimento == null) {
					$data_vencimento = $dados->data_vencimento;
				}

				if ($valor == null) {
					$valor = $dados->valor;
				}

				if ($numero == null) {
					$numero = $dados->numero;
				}

				if ($parcela == null) {
					$parcela = $dados->parcela;
				}

				$stm->bindValue(1, $data_emissao);
				$stm->bindValue(2, $data_vencimento);
				$stm->bindValue(3, $valor);
				$stm->bindValue(4, $numero);
				$stm->bindValue(5, $parcela);
				$stm->bindValue(6, $id_titulo);

				$stm->execute();
				
				echo "Dados atualizados com sucesso!<br/>";
				echo "Redirecionando para a tela de Atualizar Título em 5 segundos.";
				header("refresh:5; url=atualizarTituloForm.php");
			} else {
				echo "Este título ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Atualizar Título em 5 segundos.";
				header("refresh:5; url=atualizarTituloForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function registrar($titulo) {
		try { 
			$stm = $this->conexao->prepare("SELECT *
											FROM titulos 
											WHERE id_titulo = ?");

			$stm->bindValue(1, $titulo->id);

			$query = $stm->execute();

			if ($query) {

				$dados = $stm->fetch(PDO::FETCH_OBJ);

				$stm = $this->conexao->prepare("UPDATE titulos 
												SET data_pagamento = ?
												WHERE id_titulo = ?");

				$data_pagamento = $titulo->data_pagamento;
				$id_titulo = $titulo->id;

				$stm->bindValue(1, $data_pagamento);
				$stm->bindValue(2, $id_titulo);

				$stm->execute();
				
				echo "Dados atualizados com sucesso!<br/>";
				echo "Redirecionando para a tela de Atualizar Título em 5 segundos.";
				header("refresh:5; url=registrarPagamentoForm.php");
			} else {
				echo "Este título ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Atualizar Título em 5 segundos.";
				header("refresh:5; url=registrarPagamentoForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function visualizar() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM titulos");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "titulo ID ".$dados->id_titulo." - Numero ".$dados->numero.
						" - Data Vencimento".$dados->data_vencimento." - Data Emissao ".$dados->data_emissao."<br/>";
				}
			}

			} catch (PDOException $e) {
				echo "Erro: ".$e->getMessage();
			}
	}

	public function remover($titulo) {
		try { 
			$stm = $this->conexao->prepare("SELECT id_titulo 
											FROM titulos 
											WHERE id_titulo = ?");

			$stm->bindValue(1, $titulo->id);
			$verificaId_titulo = $stm->execute();

			if ($stm->rowCount() > 0) {
				$stm = $this->conexao->prepare("DELETE FROM titulos
												WHERE id_titulo = ?");

				$stm->bindValue(1, $titulo->id);
				$stm->execute();

				echo "Dados removidos com sucesso!<br/>";
				echo "Redirecionando para a tela de Remover Título em 5 segundos.";
				header("refresh:5; url=removerTituloForm.php");
			} else {
				echo "Este título ainda não foi cadastrado.<br/>";
				echo "Redirecionando para a tela de Remover Título em 5 segundos.";
				header("refresh:5; url=removerTituloForm.php");
			}

		} catch(PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaComboTitulos() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM titulos");
			
			$query = $stm->execute();

			if ($query) {
				echo "<option value='0'>Selecione...</option>";
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<option value='".$dados->id_titulo."'>".$dados->numero."-".$dados->parcela." ".$dados->valor." R$"."</option>";
				}
			}
		} catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaComboTitulosSemPagamento() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM titulos
											WHERE data_pagamento IS NULL");
			
			$query = $stm->execute();

			if ($query) {
				echo "<option value='0'>Selecione...</option>";
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<option value='".$dados->id_titulo."'>".$dados->numero."-".$dados->parcela." ".$dados->valor." R$"."</option>";
				}
			}
		} catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}
	}

	public function montaTabelaTitulos() {
		try {
			$stm = $this->conexao->prepare("SELECT * FROM titulos");

			$query = $stm->execute();

			if ($query) {
				while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
					echo "<tr>";
					echo "<td>".$dados->id_titulo."</td>";
					echo "<td>".$dados->numero."</td>";
					echo "<td>".$dados->parcela."</td>";
					echo "<td>".$dados->valor."</td>";
					echo "<td>".$dados->data_emissao."</td>";
					echo "<td>".$dados->data_vencimento."</td>";
					echo "<td>".$dados->data_pagamento."</td>";
					echo "<td>".$dados->id_devedor."</td>";
					echo "<td>".$dados->id_credor."</td>";
					echo "</tr>";
				}
			}
		}
		catch (PDOException $e) {
			echo "Erro: ".$e->getMessage();
		}

	}
}

?>