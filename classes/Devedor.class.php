<?php

class Devedor {

	private $id;
	private $telefone;
	protected $email;
	private $cpf_cnpj;
	private $razao_social;
	private $endereco;
	
	function __construct($id="", $telefone="", $email="", $cpf_cnpj="", $razao_social="", $endereco="") {
		
		$this->id = $id;
		$this->telefone = $telefone;
		$this->email = $email;
		$this->cpf_cnpj = $cpf_cnpj;
		$this->razao_social = $razao_social;
		$this->endereco = $endereco;
	}

	function __set($prop, $val) {
		$this->$prop = $val;
	}

	function __get($prop) {
		return $this->$prop;
	}

}

?>