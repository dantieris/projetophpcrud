<nav>
	<link rel="stylesheet" type="text/css" href="style/nav.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset -->
	
	<header>
		<h1>Menu Principal</h1>
		<ul>
			<li><a href="cadastrarCredorForm.php">Cadastrar Credor</a></li>
			<li><a href="atualizarCredorForm.php">Atualizar Credor</a></li>
			<li><a href="removerCredorForm.php">Remover Credor</a></li>
		</ul>
	</header>
</nav>