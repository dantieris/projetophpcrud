﻿<!DOCTYPE html>
<html>
<head>
	<title>Sistem Financeira - Login</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/login.css";
	</style>
</head>
<body>
	<main>
		<section>
			<figure>
				<img src="imagens/logo_login.png" alt="Logo de login da empresa.">
			</figure>

			<p>
				Informe os dados abaixo para acessar o sistema.
			</p>

			<form action="autenticar.php" method="post">
				<p>
					<label for="email">Email:</label> <input name="email" id="email" type="email" required/>
				</p>
				<p>
					<label for="senha">Senha:</label> <input name="senha" id="senha" type="password" required/>
				</p>
				<P>
					<input id="login" name="acao" type="submit" value="Login" />
				</P>
			</form>

		</section>
	</main>
	<footer>
		<p>
			© 2014 Financeira.
		</p>

		<figure>
		<a href="#">
			<img id="assinatura_logo" src="imagens/assinatura.png" alt="Logo da assinatura da empresa.">
		</a>
		</figure>
	</footer>
</body>
</html>