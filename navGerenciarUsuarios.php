<nav>
	<link rel="stylesheet" type="text/css" href="style/nav.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset -->
	
	<header>
		<h1>Menu Principal</h1>
		<ul>
			<li><a href="cadastrarUsuarioForm.php">Cadastrar Usuário</a></li>
			<li><a href="atualizarUsuarioForm.php">Atualizar Usuário</a></li>
			<li><a href="removerUsuarioForm.php">Remover Usuário</a></li>
		</ul>
	</header>
</nav>