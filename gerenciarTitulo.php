<?php
	
	// verifica se as informações vieram 
	// através do método POST
	if(isset($_POST))
	{
		require_once("classes/Titulo.class.php");
		require_once("classes/TituloDAO.class.php");
		require_once("classes/Devedor.class.php");
		require_once("classes/Usuario.class.php");
		require_once("classes/Credor.class.php");

		switch ($_POST["acao"]) {
			case 'Cadastrar':
				// recebe os valores vindos do formulário através de post
				$data_emissao = $_POST["data_emissao"];
			 	$data_vencimento = $_POST["data_vencimento"];

			 	$devedor = new Devedor($_POST["id_devedor"], "", "", "", "", "");

				$credor = new Credor($_POST["id_credor"], "", "", "", "", "");

			 	$valor = $_POST["valor"];
			 	$numero = $_POST["numero"];
			 	$parcela = $_POST["parcela"];
				
			 	$titulo = new Titulo("", $data_emissao, $data_vencimento, $devedor, $credor, $valor, $numero, $parcela, "");

				$acoes = new TituloDAO();

				if($acoes) {
					echo "Cadastrando... <br/>";
				}
				$acoes->inserir($titulo);
				break;
			case 'Atualizar' :
				// recebe os valores vindos do formulário através de post
				$data_emissao = $_POST["data_emissao"];
			 	$data_vencimento = $_POST["data_vencimento"];
			 	$id_titulo = $_POST["id_titulo"];
			 	$valor = $_POST["valor"];
			 	$numero = $_POST["numero"];
			 	$parcela = $_POST["parcela"];
				
			 	$titulo = new Titulo($id_titulo, $data_emissao, $data_vencimento, "", "", $valor, $numero, $parcela, "");

				$acoes = new TituloDAO();

				if($acoes) {
					echo "Atualizando... <br/>";
				}
				$acoes->atualizar($titulo);
				break;
			case 'Remover' :
				$id = $_POST["id_titulo"];

				$titulo = new Titulo($id, "", "", "", "", "", "", "", "");
				$acoes = new TituloDAO();

				if($acoes) {
					echo "Removendo... <br/>";
				}

				$acoes->remover($titulo);
				break;
			case 'Registrar' :
				// recebe os valores vindos do formulário através de post
				$data_pagamento = $_POST["data_pagamento"];
			 	$id_titulo = $_POST["id_titulo"];
				
			 	$titulo = new Titulo($id_titulo, "", "", "", "", "", "", "", $data_pagamento);

				$acoes = new TituloDAO();

				if($acoes) {
					echo "Atualizando... <br/>";
				}
				$acoes->registrar($titulo);
				break;
			default:
				# code...
				break;
		}
	}
?>