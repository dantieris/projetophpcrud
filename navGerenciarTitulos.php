<nav>
	<link rel="stylesheet" type="text/css" href="style/nav.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset -->
	
	<header>
		<h1>Menu Principal</h1>
		<ul>
			<li><a href="cadastrarTituloForm.php">Cadastrar Títulos</a></li>
			<li><a href="atualizarTituloForm.php">Atualizar Títulos</a></li>
			<li><a href="removerTituloForm.php">Remover Títulos</a></li>
			<li><a href="registrarPagamentoForm.php">Registrar Pagamento</a></li>
		</ul>
	</header>
</nav>