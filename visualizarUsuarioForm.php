<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Visualizar Devedor</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navVisualizarInformacoes.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<table>
				<thead>
					<tr>
						<th>ID</th>
						<th>E-mail</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td> </td>
						<td> </td>
					</tr>
				</tfoot>
				<tbody>
					<?php
						include_once("classes/UsuarioDAO.class.php");
						$acoes = new UsuarioDAO();
						$acoes->montaTabelaUsuarios();
					?>
				</tbody>
			</table>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>