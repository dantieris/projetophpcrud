﻿<?php 

	// Só pode destruir a session ou mecher nos atributos se der session_start.
	session_start();
	session_destroy();

	// Redirecionando para a tela de login.
	header("Location: login.php");

 ?>