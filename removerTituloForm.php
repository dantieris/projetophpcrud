<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Remover Devedor</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarTitulos.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarTitulo.php" method="post">
				<fieldset>
					<label for="id_titulo">Titulo
						<select name="id_titulo" id="id_titulo">
						<?php
							include "classes/TituloDAO.class.php";
							$dao = new TituloDAO();
							$dao->montaComboTitulos();
						?>
						</select>
					</label>
					<input id="remover" name="acao" type="submit" value="Remover" />
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>