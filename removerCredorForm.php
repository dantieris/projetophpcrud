<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Remover Credor</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarCredores.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarCredor.php" method="post">
				<fieldset>
					<label for="id_credor">Credor: 
						<select name="id_credor" id="id_credor">
						<?php
							include "classes/CredorDAO.class.php";
							$dao = new CredorDAO();
							$dao->montaComboCredores();
						?>
						</select>
					<input id="remover" name="acao" type="submit" value="Remover" />
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>