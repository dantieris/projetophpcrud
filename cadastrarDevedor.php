<?php
	
	// verifica se as informações vieram 
	// através do método POST
	if(isset($_POST))
	{

		include "classes/Devedor.class.php";
		include "classes/DevedorDAO.class.php";

		// recebe os valores vindos do formulário através de post
		$cpf_cnpj = $_POST["cpf_cpnj"];
		$email = $_POST["email"];
		$telefone = $_POST["telefone"];

		$devedor = new Devedor("", $telefone, $email, $cpf_cnpj);
		$acoes = new DevedorDAO();

		if($acoes) {
			echo "Cadastrando... <br/>";
		}

		$acoes->inserir($devedor);

		echo "<h1>Visualizar todos os devedores da base de dados</h1>";
		$acoes->visualizar();

	}	
?>