<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Registrar Pagamento</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
		@import "jqueryui/jquery-ui.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarTitulos.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarTitulo.php" method="post">
				<fieldset>
					<label for="id_titulo">Título: 
						<select name="id_titulo" id="id_titulo">
						<?php
							include_once("classes/TituloDAO.class.php");
							$dao = new TituloDAO();
							$dao->montaComboTitulosSemPagamento();
						?>
						</select>
					</label>

					<?php
						$useragent = $_SERVER['HTTP_USER_AGENT'];

						if(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
							$browser = "firefox";
						 } elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
							$browser = "chrome";
						}

						if ($browser == "chrome") {
							echo "<label for=\"data_pagamento\">Data de Pagamento: <input name=\"data_pagamento\" id=\"data_pagamento\" type=\"date\"/></label>";
						} elseif ($browser == "firefox") {
							echo "<label for=\"data_pagamento\">Data de Pagamento: <input class=\"data_pagamento\" name=\"data_pagamento\" id=\"data_pagamento\" type=\"text\"/></label>";
						}

					 ?>
		 			<script src="jqueryui/external/jquery/jquery.js"></script>
					<script src="jqueryui/jquery-ui.js"></script>
					<script>
						$( ".data_pagamento" ).datepicker();
					</script>

					<input id="registrar" name="acao" type="submit" value="Registrar" />
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>