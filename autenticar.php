<?php 

	if (isset($_POST)) {

		// chama de calsses para instância
		include "classes/Usuario.class.php";
		include "classes/UsuarioDAO.class.php";

		$email = $_POST["email"];
		$senha = $_POST["senha"];

		$usuario = new Usuario("", $email, $senha);
		$acoes = new UsuarioDAO();

		if (($dados = $acoes->autenticar($usuario)) == null) {
			echo "Usuario e/ou senha inválidos.<br/>";
			echo "Tente novamente.";

			header("refresh:5; url=login.php");
		} else {
			session_start();



			$_SESSION["id"] = session_id();
			$_SESSION["usuario"] = $dados;
			$_SESSION["TESTE"] = "TESTE";

			date_default_timezone_set("America/Sao_Paulo");

			$_SESSION["datahora"] = date("d/m/Y H:i:s");

			header("Location: main.php");
		}

	}
	

 ?>