<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Cadastrar Devedor</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarDevedores.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarDevedor.php" method="post">
				<fieldset>
					<label for"razao_social">Razão Social: <input name="razao_social" id="razao_social" type="text"/></label>
					
					<label for"endereco">Endereço: <input name="endereco" id="endereco" type="text"/></label>

					<label for"cpf_cnpj">CPF / CNPJ: <input name="cpf_cpnj" id="cpf_cpnj" type="text"/></label>

					<label for="email">Email: <input name="email" id="email" type="email" required/></label>

					<label for"telefone">Telefone: <input name="telefone" id="telefone" type="text"/></label>

					<input id="cadastrar" name="acao" type="submit" value="Cadastrar" />
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>