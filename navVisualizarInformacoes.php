<nav>
	<link rel="stylesheet" type="text/css" href="style/nav.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset -->
	
	<header>
		<h1>Menu Principal</h1>
		<ul>
			<li><a href="visualizarCredorForm.php">Visualizar Credores</a></li>
			<li><a href="visualizarDevedorForm.php">Visualizar Devedores</a></li>
			<li><a href="visualizarUsuarioForm.php">Visualizar Usuários</a></li>
			<li><a href="visualizarTituloForm.php">Visualizar Títulos</a></li>
		</ul>
	</header>
</nav>