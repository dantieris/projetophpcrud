<?php
	
	// verifica se as informações vieram 
	// através do método POST
	if(isset($_POST))
	{
		require_once("classes/Credor.class.php");
		require_once("classes/CredorDAO.class.php");

		switch ($_POST["acao"]) {
			case 'Cadastrar':
				// recebe os valores vindos do formulário através de post
				$cpf_cnpj = $_POST["cpf_cpnj"];
				$email = $_POST["email"];
				$telefone = $_POST["telefone"];
				$razao_social = $_POST["razao_social"];
				$endereco = $_POST["endereco"];

				$credor = new Credor("", $cpf_cnpj, $email, $telefone, $razao_social, $endereco);
				$acoes = new CredorDAO();

				if($acoes) {
					echo "Cadastrando... <br/>";
				}

				$acoes->inserir($credor);
				break;
			case 'Atualizar' :
					// recebe os valores vindos do formulário através de post
					$cpf_cnpj = $_POST["cpf_cpnj"];
					$email = $_POST["email"];
					$telefone = $_POST["telefone"];
					$razao_social = $_POST["razao_social"];
					$endereco = $_POST["endereco"];
					$id = $_POST["id_credor"];
					
					$credor = new Credor($id, $cpf_cnpj, $email, $telefone, $razao_social, $endereco);
					$acoes = new CredorDAO();

					if($acoes) {
						echo "Atualizando... <br/>";
					}

					$acoes->atualizar($credor);
				break;
			case 'Remover' :
					$id = $_POST["id_credor"];

					$credor = new Credor($id, "", "", "", "", "");
					$acoes = new CredorDAO();

					if($acoes) {
						echo "Removerndo... <br/>";
					}

					$acoes->remover($credor);
				break;
			default:
				# code...
				break;
		}
	}
?>