<nav>
	<link rel="stylesheet" type="text/css" href="style/nav.css"> <!-- Importando css -->
	<meta charset="UTF-8"> <!-- Definindo charset -->
	
	<header>
		<h1>Menu Principal</h1>
		<ul>
			<li><a href="gerenciarCredoresForm.php">Gerenciar Credores</a></li>
			<li><a href="gerenciarDevedoresForm.php">Gerenciar Devedores</a></li>
			<li><a href="gerenciarUsuariosForm.php">Gerenciar Usuários</a></li>
			<li><a href="gerenciarTitulosForm.php">Gerenciar Títulos</a></li>
			<li><a href="visualizarInformacoesForm.php">Visualizar Informações</a></li>
		</ul>
	</header>
</nav>