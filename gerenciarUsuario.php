<?php
	
	// verifica se as informações vieram 
	// através do método POST
	if(isset($_POST))
	{
		require_once("classes/Usuario.class.php");
		require_once("classes/UsuarioDAO.class.php");

		switch ($_POST["acao"]) {
			case 'Cadastrar':
				// recebe os valores vindos do formulário através de post
				$email = $_POST["email"];
				$senha = $_POST["senha"];
				$senha_repetida = $_POST["senha_repetida"];

				if ($senha === $senha_repetida) {
					$usuario = new Usuario("", $email, $senha);
					$acoes = new UsuarioDAO();

					if($acoes) {
						echo "Cadastrando... <br/>";
					}

					$acoes->inserir($usuario);
				} else {
					echo "Erro:  As senhas devem ser iguais.<br/>";
					echo "Redirecionando para a tela de Cadastrar Usuário em 5 segundos.";
					header("refresh:5; url=cadastrarUsuarioForm.php");
				}

				break;
			case 'Atualizar' :
				// recebe os valores vindos do formulário através de post
				$senha = $_POST["senha"];
				$senha_repetida = $_POST["senha_repetida"];

				if ($senha === $senha_repetida) {
					$usuario = new Usuario("", "", $senha);
					$acoes = new UsuarioDAO();

					if($acoes) {
						echo "Cadastrando... <br/>";
					}

					$acoes->atualizar($usuario);
				} else {
					echo "Erro:  As senhas devem ser iguais.<br/>";
					echo "Redirecionando para a tela de Atualizar Usuário em 5 segundos.";
					header("refresh:5; url=atualizarUsuarioForm.php");
				}
				break;
			case 'Remover' :
					$id = $_POST["id_usuario"];

					$usuario = new Usuario($id, "", "");
					$acoes = new UsuarioDAO();

					if($acoes) {
						echo "Removerndo... <br/>";
					}

					$acoes->remover($usuario);
				break;
			default:
				# code...
				break;
		}
	}
?>