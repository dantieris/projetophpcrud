<!DOCTYPE html>
<html>
<head>
	<title>Sistema Financeira - Atualizar Devedor</title>
	<meta charset="UTF-8">

	<style type="text/css">
		@import "style/estilo.css";
		@import "jqueryui/jquery-ui.css";
	</style>
</head>
<body>
	<?php 
		include_once("header.php"); // Importando o cabeçalho da página.
		include_once("navGerenciarTitulos.php"); // Importandp a barra de navegação da página.
	 ?>
	<main>
		<div id="status"></div>
		<section>
			<form action="gerenciarTitulo.php" method="post">
				<fieldset>
					<label for="id_titulo">Título: 
						<select name="id_titulo" id="id_titulo">
						<?php
							include "classes/TituloDAO.class.php";
							$dao = new TituloDAO();
							$dao->montaComboTitulos();
						?>
						</select>
					</label>
					<!-- CÓDIGO PHP PARA TRATAR O HTML NO CHROME OU NO FIREFOX -->
					<?php
						$useragent = $_SERVER['HTTP_USER_AGENT'];

						if(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
							$browser = "firefox";
						 } elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
							$browser = "chrome";
						}

						if ($browser == "chrome") {
							echo "<label for=\"data_emissao\">Data Emissao: <input name=\"data_emissao\" id=\"data_emissao\" type=\"date\" required/></label>";
							echo "<label for=\"data_vencimento\">Data Vencimento: <input name=\"data_vencimento\" id=\"data_vencimento\" type=\"date\" required/></label>";						
						} elseif ($browser == "firefox") {
							echo "<label for=\"data_emissao\">Data Emissao: <input class=\"datepicker\" name=\"data_emissao\" id=\"data_emissao\" type=\"date\" required/></label>";
							echo "<label for=\"data_vencimento\">Data Vencimento: <input class=\"datepicker\" name=\"data_vencimento\" id=\"data_vencimento\" type=\"date\" required/></label>";
						}

					 ?>
					<label for="valor">Valor: <input name="valor" id="valor" type="text"/></label>

					<label for="numero">Numero: <input name="numero" id="numero" type="number"/></label>

					<label for="parcela">Parcela: <input name="parcela" id="parcela" type="number"></label>

					<!-- SCRIPT PARA TRATAR O DATE PICKER COM O JQUERY -->		
					<script src="jqueryui/external/jquery/jquery.js"></script>
					<script src="jqueryui/jquery-ui.js"></script>
					<script>
						$( ".datepicker" ).datepicker();
					</script>

					<input id="atualizar" name="acao" type="submit" value="Atualizar" />
				</fieldset>
			</form>
		</section>
	</main>
	<?php 
		include_once("footer.php"); // Importando o rodapé da página.
	 ?>
</body>
</html>